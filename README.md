# voctrain
A dead-simple vocabular training cli written in Python

## Support
Currently **only** UNIX like systems are supported. (no windows, maybe macOS will work, but made for linux)

## Installation / Run
### Bare metal
 * Optionally create a virtual environment and activate it.
 * `pip3 install -r requirements.txt`

### Preview
[![asciicast](https://asciinema.org/a/GDhBg5hvuj9etXySEOgStV2Bc.png)](https://asciinema.org/a/GDhBg5hvuj9etXySEOgStV2Bc)
 
### Usage
`python3 main.py train <collectionname> [--amount 10]`:

Trains all words in the collection


`python3 main.py add <collectionname>`:

Opens an interactive mode to add new words

