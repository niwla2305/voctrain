import os
import random
from pathlib import Path
import colorama

import click
import csv


class Collection:
    def __init__(self, collection):
        self.config = []
        self.collection = collection
        self.read()

    def read(self):
        try:
            with open(f"{os.environ['HOME']}/.config/voctrain/{self.collection}.csv", "r", newline='') as f:
                config = []
                reader = csv.reader(f)
                for line in reader:
                    if line[0] != "\n" or "":
                        config.append({"language1": line[0], "language2": line[1]})
                self.config = config
        except FileNotFoundError:
            os.makedirs(f"{os.environ['HOME']}/.config/voctrain/", exist_ok=True)
            Path(f"{os.environ['HOME']}/.config/voctrain/{self.collection}.csv").touch()
            self.read()

    def write(self):
        with open(f"{os.environ['HOME']}/.config/voctrain/{self.collection}.csv", "w", newline='') as f:
            content = ""
            writer = csv.writer(f)
            for entry in self.config:
                content = f"{content}\n{entry['language1']},{entry['language2']}"
                writer.writerow([entry["language1"], entry["language2"]])

    def __getitem__(self, item):
        return self.config[item]

    def append(self, language1, language2):
        self.config.append({"language1": language1, "language2": language2})


@click.group()
def main():
    """
    Simple CLI for querying books on Google Books by Oyetoke Toby
    """
    colorama.init()


correct = 0
incorrect = 0


@main.command()
@click.argument('collection')
@click.option('--amount', default=-1, type=int)
def train(collection, amount):
    global correct
    global incorrect
    os.system('cls' if os.name == 'nt' else 'clear')
    collection = Collection(collection)
    collection_data = collection.config
    if amount != -1:
        collection_data = collection_data[:amount]
    click.secho(f"Starting quiz with {len(collection_data)} questions.", fg="yellow")
    random.shuffle(collection_data)
    for entry in collection_data:
        click.secho("Question:", fg="green")
        click.echo(entry["language1"])
        input(click.style("[Press enter to show]", fg="bright_black"))
        click.secho("Answer:", fg="green")
        click.echo(entry["language2"])

        def ask_correct():
            result = click.prompt(click.style("Did you know it? [y,n]", fg="bright_black"))
            if result.lower() == "y":
                global correct
                correct = correct + 1
            elif result.lower() == "n":
                global incorrect
                incorrect = incorrect + 1
            else:
                ask_correct()

        ask_correct()
        os.system('cls' if os.name == 'nt' else 'clear')

    click.secho(f"Correct: {correct}\nIncorrect: {incorrect}", blink=True, bg="yellow", fg="black")


@main.command()
@click.argument('collection')
@click.option('--language1', prompt=True)
@click.option('--language2', prompt=True)
def add(collection, language1, language2):
    collection = Collection(collection)
    collection.append(language1, language2)
    collection.write()
    click.echo("Done.")
    add()


if __name__ == "__main__":
    main()
